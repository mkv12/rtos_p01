package test;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	public Stage rootStage;
	public AnchorPane rootPane;
	private RootController controlRoot;
	
	@Override
	public void start(Stage primaryStage) {
		this.rootStage = primaryStage;
		this.rootStage.setTitle("P01");
		this.rootStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we){
				try {
					Alert close = new Alert(Alert.AlertType.CONFIRMATION);
					close.setHeaderText("Saliendo...");
					close.setContentText("Desea Cerrar la Aplicacion?");
					
					close.showAndWait().ifPresent(response -> {
						if ( response == ButtonType.OK ) {
							Platform.exit();
						} else {
							we.consume();
						}
					});
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		});
		
		initRootView();
	}
	
	public void initRootView() {
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("RootView.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();
			this.rootStage.setScene(new Scene(anchorPane));
			
			/* Load Controller */
			this.controlRoot = loader.getController();
			this.controlRoot.setMainApp(this);
			
			/* Display */
			this.rootStage.show();		
			
		} catch(IOException ioe) {
			System.err.println(ioe);
		}
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
