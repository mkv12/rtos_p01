package test;

import java.util.Random;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class RootController {
	private Main mainApp;
	
	@FXML private Button btnRun;
	@FXML private Button btnStep;
	@FXML private Button btnReset;
	@FXML private TextArea txtConsole;
	@FXML private Label lblGlobalTime;
	@FXML private Label lblQueue;
	@FXML private Label lblAttentionTime;
	@FXML private Label lblCheckTime;
	@FXML private Label lblProbX;
	@FXML private Label lblProbY;
	
	private static int GLOBAL_TIME = 480;
	private static int CLIENT_QUEUE = 0;
	private static int QUEUE_TIME = 20;
	
	private static int COUNT_TIME = GLOBAL_TIME ;
	private static int ATTENTION_TIME = 0;
	private static int RECHECK_QUEUE_TIME = 0;
	
	private static boolean IS_CLIENT_ATTENDED = false;
	private static boolean WAS_ALREADY_EMPTY = false;
	private static boolean STOP = false;
	
	private static final Random O_RAND = new Random();
	
	@FXML private void initialize() {
		this.txtConsole.appendText("Iniciado \n");
	}
	
	@FXML private void step() {
		if(COUNT_TIME > 0) {
			action();
		} else {
			if(!STOP) {
				this.txtConsole.appendText(COUNT_TIME +": Final: \n");
				this.txtConsole.appendText("***********************\n");
				this.txtConsole.appendText("Tiempo faltante de Atender: "+(ATTENTION_TIME)+"\n");
				this.txtConsole.appendText("Clientes por Atender: "+(CLIENT_QUEUE)+"\n");
				STOP = true;
			}
		}

	}
	
	@FXML private void runall() {
		
	}
	
	@FXML private void configure() {
		
	}
	
	@FXML private void reset() {
		COUNT_TIME = GLOBAL_TIME ; 
		CLIENT_QUEUE = 0;
		ATTENTION_TIME = 0;
		RECHECK_QUEUE_TIME = 0;
		STOP = false;
		IS_CLIENT_ATTENDED = false;
		WAS_ALREADY_EMPTY = false;
		
		this.lblGlobalTime.setText(Integer.toString(COUNT_TIME));
		this.lblQueue.setText(Integer.toString(CLIENT_QUEUE));
		this.lblAttentionTime.setText(Integer.toString(ATTENTION_TIME));
		this.lblCheckTime.setText(Integer.toString(RECHECK_QUEUE_TIME));
		this.lblProbX.setText(Double.toString(0.0));
		this.lblProbY.setText(Double.toString(0.0));
		
		this.txtConsole.appendText("\nReiniciado\n");
	}
	
	private void action() {
		--COUNT_TIME;
		
		double x = O_RAND.nextDouble();
		double y = O_RAND.nextDouble();
		
		if(RECHECK_QUEUE_TIME > 0) {
			--RECHECK_QUEUE_TIME;
		} else {
			if( x > 0.5 ) {
				++CLIENT_QUEUE;
				WAS_ALREADY_EMPTY = false;
				this.txtConsole.appendText((COUNT_TIME + 1)+": Nuevo cliente. | Prob: "+Double.toString(x)+"\n");
			} else {
				this.txtConsole.appendText((COUNT_TIME + 1)+": No entra cliente. | Prob: "+Double.toString(x)+"\n");
			}
			RECHECK_QUEUE_TIME = QUEUE_TIME - 1;
		}
		
		if(ATTENTION_TIME > 0) {
			--ATTENTION_TIME;
		} else { 
			if(IS_CLIENT_ATTENDED) {
				IS_CLIENT_ATTENDED = false;
				this.txtConsole.appendText((COUNT_TIME + 1)+": Sale cliente \n");
			} else {
				
			}
			
			if(CLIENT_QUEUE > 0 ) {
				--CLIENT_QUEUE;
				IS_CLIENT_ATTENDED = true;
				ATTENTION_TIME = (int) (y * 10.0 + 20.0) - 1;
				this.txtConsole.appendText(COUNT_TIME+": Cliente es atendido. | Tiempo Asignado: "+(ATTENTION_TIME + 1)+"\n");
			} else {
				if(WAS_ALREADY_EMPTY) {
					
				} else {
					this.txtConsole.appendText(COUNT_TIME+": Cola Vacia. \n");
					WAS_ALREADY_EMPTY = true;
				}
				
			}
		}
		
		this.lblGlobalTime.setText(Integer.toString(COUNT_TIME));
		this.lblQueue.setText(Integer.toString(CLIENT_QUEUE));
		this.lblAttentionTime.setText(Integer.toString(ATTENTION_TIME));
		this.lblCheckTime.setText(Integer.toString(RECHECK_QUEUE_TIME));
		this.lblProbX.setText(Double.toString(x));
		this.lblProbY.setText(Double.toString(y));
	}

	public Main getMainApp() {
		return mainApp;
	}

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}
}
